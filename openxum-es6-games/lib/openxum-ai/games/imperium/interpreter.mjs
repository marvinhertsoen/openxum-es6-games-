"use strict";

require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const Parser = require('jison').Parser;

const grammar = {
    "lex": {
        "macros": {
            "id": "[_a-zA-Z]+",
            "real" : "[0-9]+\\.[0-9]*", // a changer
            "int": "(?:[0-9]|[1-9][0-9]+)"
        },
        "rules": [
            ["\\s+", "/* skip whitespace */"],
            ["\\/\\/.*", "/* ignore comment */"],
            ["entities\\b", "return 'SECTION_ENTITIES';"],
            [":", "return ':';"],
            [";", "return ';';"],
            [",", "return ',';"],
            ["=", "return '=';"],
            ["\\<", "return '<';"],
            ["\\>", "return '>';"],
            ["\\[", "return '[';"],
            ["\\]", "return ']';"],
            ["\\(", "return '(';"],
            ["\\)", "return ')';"],
            ["\\{", "return '{';"],
            ["\\}", "return '}';"],

            ["\\*", "return '*';"], // ?
            ["\\+", "return '+';"], // ?
            ["\\/", "return '/';"], // ?
            ["-", "return '-';"],


            ["arg max", "return 'ARG_MAX';"],
            ["arg min", "return 'ARG_MIN';"],
            ["max", "return 'MAX';"],
            ["min", "return 'MIN';"],
            ["sum", "return 'SUM';"],
            ["mean", "return 'MEAN';"],
            ["card", "return 'CARD';"],
            ["in", "return 'IN';"],

            ["move_groups", "return 'MOVE_GROUPS';"],
            ["out_ways", "return 'OUT_WAYS';"],
            ["in_ways", "return 'IN_WAYS';"],
            ["locations", "return 'LOCATIONS';"],
            ["groups", "return 'GROUPS';"],
            ["capabilities", "return 'CAPABILITIES';"],
            ["move_capabilities", "return 'MOVE_CAPABILITIES';"],
            ["interactions", "return 'INTERACTIONS';"],

            ["location", "return 'LOCATION';"],
            ["type", "return 'TYPE';"],
            ["interaction", "return 'INTERACTION';"],
            ["capability", "return 'CAPABILITY';"],
            ["move_capability", "return 'MOVE_CAPABILITY';"],
            ["group_size", "return 'GROUP_SIZE';"],

            ["if", "return 'IF';"],

            ["exists", "return 'EXISTS';"],
            ["new", "return 'NEW';"],
            ["move", "return 'MOVE';"],
            ["no_move", "return 'NO_MOVE';"],

            ["{real}", "return 'REAL';"],
            ["{id}", "return 'ID';"],
            ["{int}\\b", "return 'INTEGER';"],
            [".", "/* ignore bad characters */"]
        ]
    },
    "tokens": "SECTION_ENTITIES : ; , = < > [ ] ( ) { } * + - ARG_MAX ARG_MIN MAX MIN SUM MEAN CARD IN MOVE_GROUPS " +
        "OUT_WAYS IN_WAYS LOCATIONS GROUPS CAPABILITIES MOVE_CAPABILITIES INTERACTIONS LOCATION TYPE "+
        "INTERACTION CAPABILITY MOVE_CAPABILITY GROUP_SIZE IF EXISTS NEW MOVE NO_MOVE REAL ID INTEGER /",
    "start": "Instructions",
    "bnf": {
        "Instructions": ["Instruction", "Instructions Instruction"],
        "Instruction": ["AssignmentInstruction", "ConditionalInstruction", "MoveCreation"],
        "AssignmentInstruction": [
            ["Variable = Expression", "yy.ast.push(new yy.types.AssignmentInstruction(yy.ast.pop(),yy.ast.pop()));"]
        ],
        "Variable": [
            ["Tuple", "yy.ast.push(new yy.types.VARIABLE(yy.ast.pop()));"],
            ["ID", "yy.ast.push(new yy.types.VARIABLE($1));"]
        ],
        "Tuple": [
            ["( Ids )", "yy.ast.push(new yy.types.TUPLE(yy.ast.pop()));"]

        ],
        "Ids": [
            ["Ids , ID","yy.ast[yy.ast.length - 1]. push(new yy.types.ID($3));"],
            ["ID","yy.ast.push([new yy.types.ID($1)]);"]
        ],

        "Expression" : [
            ["Term * Term", ""],
            ["Term / Term", ""],
            ["Term",""]
        ],
        "Term" : [
            ["Factor + Factor",""],
            ["Factor - Factor",""],
            ["Factor",""]
        ],
        "Factor" : [
            ["ID", "yy.ast.push(new yy.types.ID($1));"],
            ["INTEGER", ""],
            ["REAL", ""],
            ["Function", ""],
            ["( expression )", ""]
        ],
        "SetUnaryOperator" : [
            ["ARG_MAX", ""],
            ["ARG_MIN", ""],
            ["MAX", ""],
            ["MIN", ""],
            ["SUM", ""],
            ["MEAN", ""],
            ["CARD", ""],
        ],
        "SetDefinitions" : [
            ["SetDefinitions , SetDefinition", " var l = yy.ast.pop(); yy.ast[yy.ast.length - 1].push(l);"],
            ["SetDefinition", "yy.ast.push([yy.ast.pop()]);"]
        ],
        "SetDefinition" : [
            ["ID IN Set", "yy.ast.push(new yy.types.SetDefinition($1, $2, yy.ast.pop()));"]
        ],
        "Set" : [
            ["SetFunction ( Parameters )", "yy.ast.push(new yy.types.Set($1,yy.ast.pop()));"]
        ],
        "SetFunction": [
            ["MOVE_GROUPS", ""],
            ["OUT_WAYS", ""],
            ["IN_WAYS", ""],
            ["LOCATIONS", ""],
            ["GROUPS", ""],
            ["ENTITIES", ""],
            ["CAPABILITIES", ""],
            ["MOVE_CAPABILITIES", ""],
            ["INTERACTIONS", ""]
        ],
        "Parameters" : [
            ["Parameters , Parameter", "var l = yy.ast.pop(); yy.ast[yy.ast.length-1].push(new yy.types.Parameter(l));"],
            ["Parameter", "yy.ast.push([new yy.types.Parameter(yy.ast.pop())]);"]
        ],
        "Parameter" : [
            ["*", "yy.ast.push($1);"],
            ["Expression", ""]
        ],
        "Function" : [
            ["SetUnaryOperator [ SetDefinitions ] ( Expression )", "yy.ast.push(new yy.types.Function3args(yy.ast.pop(), yy.ast.pop(), $1))"],
            ["FunctionName ( Parameters )", "yy.ast.push(new yy.types.Function2args(yy.ast.pop(), $1));"]
        ],
        "FunctionName": [
            ["LOCATION", ""],
            ["TYPE", ""],
            ["INTERACTION", ""],
            ["CAPABILITY", ""],
            ["MOVE_CAPABILITY", ""],
            ["GROUP_SIZE", ""]
        ],
        "ConditionalInstruction" : ["IF ( Condition ) Block Block"],
        "Block" : ["[ Instructions ]"],
        "Condition" : ["ID EXISTS"],
        "MoveCreation" : ["NEW MOVE ( ID , ID )", "NEW NO_MOVE"]
    }
};

class ID {
    constructor(id) { this._id = id; }
    to_string() { return this._id; }
}
class TUPLE {
    constructor(list) {     this._list = list; }
    to_string () {          return '(' + this._list.map(e => e._id).join(", ") + ')'; }
}
class VARIABLE {
    constructor(item) {     this._item = item; }
    to_string() {           return this._item.to_string(); }
}
class Function{
    constructor(parameters, functionname) {     this._parameters = parameters; this._functionname = functionname;  }
    to_string() {           return "FUNCTION (" + this._parameters.to_string()+" " +this._functionname.to_string()+ ")"; }
}

class Parameter {
    constructor(item) { this._item = item; }
    to_string() {
        if   ( this._item === "*") { return this._item; }
        else { return this._item.to_string();           }
    }
}

class SetDefinition {
    constructor(id, in_, set_) {
        this._id = id;
        this._in = in_;
        this._set = set_;
    }
    to_string() {
        return this._id + " " + this._in + " " + this._set.to_string();
    }
}

class Set{
    constructor(setFunction, parameters ) {     this._setFunction = setFunction; this._parameters = parameters }
    to_string() {            return this._setFunction + '(' +this._parameters.map(param => param.to_string()).join(", ") +')'; }
}

class Function3args {
    constructor(expression, setDefinitions, setUnaryOperator) {
        this._expression = expression;
        this._setDefinitions = setDefinitions;
        this._setUnaryOperator = setUnaryOperator;
    }

    to_string() {
        return this._setUnaryOperator + " [" + this._setDefinitions.map(param => param.to_string()).join(", ") + "](" +
            this._expression.to_string() + ") ";
    }
}

class Function2args {
    constructor(parameters, functionName) {
        this._parameters = parameters;
        this._functionName = functionName;
    }
    to_string() {
        return this._functionName + "(" + this._parameters.map(param => param.to_string()).join(", ") + ") ";
    }
}

class AssignmentInstruction {
    constructor(expression,variable) {
        this._variable = variable;
        this._expression = expression;
    }
    to_string() {
        return this._variable.to_string() + " = " + this._expression.to_string();
    }
    evaluate (){
        /*
            evaluate recursive this._expression , get object with 2 valuse (g,w) & inject values in this._variable
            evaluate this._variable ?
         */
    }

}


class If {
    constructor(condition , then_block , else_block) {
        this._condition = condition;
        this._then_block = then_block;
        this._else_block = else_block;
    }
    check_type(symbol_table) {
        this._condition.check_type(symbol_table);
        this._then_block.check_type(symbol_table );
        this._else_block.check_type(symbol_table );
        if(this._condition.type(symbol_table ).type() !==  Types.BOOLEAN) {
            throw  newTypeError("Error  type of  condition  of If");
        }
    }
    evaluate(context) {
        /*
        this._symbol_table.clear ();
        ast.evaluate ({ symbol_table:this._symbol_table , engine:this._engine });
         */
        if(this._condition.evaluate(context )) {
            return  this._then_block.evaluate(context );
        }
        else{
            return  this._else_block.evaluate(context );}
    }
}


class Interpreter {
  constructor(engine) {
    this._engine = engine;
    this._symbol_table =  new Map();
  }

  // public methods
  move_script() {

      const options = { type : "lalr " , moduleType : "commonjs", moduleName : "scripting_player"};
      /*
            const script = " (g , w ) = arg max [ g in move_groups (*) , " +
              " w in out_ways ( location ( g ))]( move_capability (g , type ( w )) " +
              " / ( group_size ( g ) * 100)) " +
              " if ( g exists ) [ new move (g , w ) ] [ new no_move ] " ;

            const Lex = require('jison-lex');
            const lexer = new Lex(grammar.lex);

            lexer.setInput(script);

            let current;
            while(current !== 1){
                current = lexer.lex();
                process.stdout.write(current + " ")
            }
            console.log("");
       */

        /*
            let parser = new Parser (grammar, options);
            parser.parse(script);
         */

      /*const script = " (g , w ) = arg max [ g in move_groups (*) , w in out_ways ( location ( g ))] ( move_capability (g , type ( w )) / ( group_size ( g ) * 100)) " +
      " if ( g exists ) [ new move (g , w ) ] [ new no_move ] " ;*/
      //const script = " (g , w ) = arg max [ g in move_groups (*) , w in out_ways ( location ( g ))] ( move_capability (g , type ( w ))) ";

      const script = "(g , w ) = arg max [ g in move_groups (*) , w in out_ways ( location ( g ))] ( move_capability (g , type ( w ))) ";
      //"expansion": [["Term * Term", "yy.ast.push(new Multi...)"],[...]]

      let parser = new Parser(grammar, options);
      parser.yy.types = {
          ID : ID,
          TUPLE : TUPLE,
          VARIABLE : VARIABLE,
          Parameter : Parameter,
          Function : Function,
          Set : Set,
          Function3args : Function3args,
          Function2args : Function2args,
          SetDefinition : SetDefinition,
          AssignmentInstruction : AssignmentInstruction
      };
      parser.yy.ast = [];
      parser.parse(script);

      console.log(parser.yy.ast);
      this._evaluate(parser.yy.ast[0]);

    return;
      let list = this._engine.get_possible_move_list();

    return list[Math.floor(Math.random() * list.length)];
  }

  interaction_script() {
    let list = this._engine.get_possible_move_list();

    return list[Math.floor(Math.random() * list.length)];
  }
  _evaluate(ast) {
      console.log(this._symbol_table);
      this._symbol_table.clear();
      ast.evaluate ({ symbol_table:this._symbol_table , engine:this._engine });
  }
}

export default Interpreter;
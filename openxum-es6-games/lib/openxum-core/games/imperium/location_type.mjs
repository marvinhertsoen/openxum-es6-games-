"use strict";

const LocationType = {
  HIGH_MOUNTAIN: 0, MIDDLE_MOUNTAIN: 1, LOW_LAND: 2, JUNGLE: 3,
  SWAMP: 4, PACK_ICE: 5, DESERT: 6, FOREST: 7, ISLAND: 8, BUILDING: 9
};

export default LocationType;
"use strict";

import Clan from './clan.mjs';
import Group from "./group.mjs";
import Location from "./location.mjs";
import LocationType from "./location_type.mjs";
import Map from './map.mjs';
import Way from "./way.mjs";
import WayType from "./way_type.mjs";

class Builder {
  constructor() {
  }

  build_clan(color, entities) {
    let clan = new Clan(color);

    for (let i = 0; i < 5; ++i) {
      let group = new Group(color, i);

      for (let j = 0; j < 5; ++j) {
        const index = Math.floor(Math.random() * entities.length);

        group.add_entity(entities[index].clone());
        entities.splice(index, 1);
      }
      clan.add_group(group);
    }
    return clan;
  }

  build_map(location_number) {
    const location_types = Object.keys(LocationType);
    const way_types = Object.keys(WayType);
    let locations = [];
    let ways = [];
    let matrix = Array(location_number);

    for (let i = 0; i < location_number; ++i) {
      matrix[i] = Array(location_number).fill(0);
    }
    for (let i = 0; i < location_number; ++i) {
      locations.push(new Location(i, location_types[Math.floor(Math.random() * location_types.length)]));
    }
    for (let i = 0; i < location_number; ++i) {
      let way_number = Math.floor(Math.random() * ((location_number - i) / 2)) + 1;
      let indexes = [];

      for (let k = i + 1; k < location_number; ++k) {
        indexes.push(k);
      }
      way_number = way_number > indexes.length ? Math.ceil(indexes.length / 2) : way_number;
      for (let k = 0; k < way_number; ++k) {
        const j = Math.floor(Math.random() * indexes.length);

        matrix[i][indexes[j]] = 1;
        indexes.splice(j, 1);
      }
    }
    for (let j = 1; j < location_number; ++j) {
      let i = 0;
      let found = false;

      while (!found && i < j) {
        if (matrix[i][j] === 1) {
          found = true;
        } else {
          ++i;
        }
      }
      if (!found) {
        matrix[Math.floor(Math.random() * j)][j] = 1;
      }
    }
    for (let i = 0; i < location_number; ++i) {
      for (let j = i + 1; j < location_number; ++j) {
        if (matrix[i][j] !== 0) {
          if (locations[i].type() === 'ISLAND' || locations[j].type() === 'ISLAND') {
            ways.push(new Way('SEA', i, j));
            ways.push(new Way('SEA', j, i));
          } else {
            const type = way_types[Math.floor(Math.random() * way_types.length)];

            ways.push(new Way(type, i, j));
            ways.push(new Way(type, j, i));
          }
        }
      }
    }
    return new Map(locations, ways);
  }
}

export default Builder;
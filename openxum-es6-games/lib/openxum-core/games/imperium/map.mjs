"use strict";

class Map {
  constructor(locations, ways) {
    this._locations = locations;
    this._ways = ways;
  }

  get_way(from, to) {
    let i = 0;
    let found = false;

    while (!found && i < this._ways.length) {
      if (this._ways[i].from() === from && this._ways[i].to() === to) {
        found = true;
      } else {
        ++i;
      }
    }
    if (found) {
      return this._ways[i];
    } else {
      return null;
    }
  }

  in_ways(location) {
    let list = [];
    let i = 0;

    while (i < this._ways.length) {
      if (this._ways[i].to() === location.index()) {
        list.push(this._ways[i]);
      }
      ++i;
    }
    return list;
  }

  locations() {
    return this._locations;
  }

  out_ways(location) {
    let list = [];
    let i = 0;

    while (i < this._ways.length) {
      if (this._ways[i].from() === location.index()) {
        list.push(this._ways[i]);
      }
      ++i;
    }
    return list;
  }

  to_string() {
    let str = "";

    this._locations.forEach((e) => {
      str += e.to_string() + "\n";
    });
    str += "\n";
    this._ways.forEach((e) => {
      str += e.to_string() + "\n";
    });
    return str;
  }

  ways() {
    return this._ways;
  }
}

export default Map;
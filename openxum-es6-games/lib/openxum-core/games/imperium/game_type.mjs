"use strict";

const GameType = {
  TWO_PLAYERS: 0, THREE_PLAYERS: 1, FOUR_PLAYERS: 2, FIVE_PLAYERS: 3,
  SIX_PLAYERS: 4, SEVEN_PLAYERS: 5, EIGHT_PLAYERS: 6
};

export default GameType;
"use strict";

const MoveType = {MOVE: 0, INTERACTION: 1, ABORT: 2};

export default MoveType;
"use strict";

import CapabilityType from './capability_type.mjs';

class Interaction {
  constructor() {
    this._effects = {};
    Object.keys(CapabilityType).forEach((key) => {
      this._effects[key] = 0;
    });
  }
}

export default Interaction;
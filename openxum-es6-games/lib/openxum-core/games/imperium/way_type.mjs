"use strict";

const WayType = {
  HIGH_MOUNTAIN: 0, MIDDLE_MOUNTAIN: 1, LOW_LAND: 2, JUNGLE: 3,
  SWAMP: 4, PACK_ICE: 5, DESERT: 6, FOREST: 7, SEA: 8, ROAD: 9
};

export default WayType;
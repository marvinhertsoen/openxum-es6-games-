require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../lib/openxum-core/').default;
const AI = require('../lib/openxum-ai/').default;

let black_wins = 0;
let white_wins = 0;

for (let i = 0; i < 10; ++i) {
  let e = new OpenXum.Imperium.Engine(OpenXum.Imperium.GameType.TWO_PLAYERS, OpenXum.Imperium.Color.BLACK);
  //let p1 = new AI.Generic.RandomPlayer(OpenXum.Imperium.Color.WHITE, OpenXum.Imperium.Color.BLACK, e);
//  let p1 = new AI.Specific.Imperium.RandomPlayer(OpenXum.Imperium.Color.WHITE, OpenXum.Imperium.Color.BLACK, e);
  let p1 = new AI.Specific.Imperium.ScriptingPlayer(OpenXum.Imperium.Color.WHITE, OpenXum.Imperium.Color.BLACK, e);
  let p2 = new AI.Generic.RandomPlayer(OpenXum.Imperium.Color.WHITE, OpenXum.Imperium.Color.BLACK, e);
  let p = p1;

  while (!e.is_finished()) {
    e.move(p.move());
    if (e.current_color() === OpenXum.Imperium.Color.BLACK) {
      p = p1;
    } else {
      p = p2;
    }
  }
  if (e.winner_is() === OpenXum.Imperium.Color.BLACK) {
    black_wins++;
  } else {
    white_wins++;
  }
}

console.log("BLACK = " + black_wins + " / WHITE = " + white_wins);
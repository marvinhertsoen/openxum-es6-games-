import Abande from './abande/index.mjs';
import Dakapo from './dakapo/index.mjs';
import Dvonn from './dvonn/index.mjs';
import Imperium from './imperium/index.mjs';
import Invers from './invers/index.mjs';
import Kikotsoka from './kikotsoka/index.mjs';
import Neutreeko from "./neutreeko/index.mjs";

export default {
  Abande: Abande,
  Dakapo: Dakapo,
  Dvonn: Dvonn,
  Imperium: Imperium,
  Invers: Invers,
  Kikotsoka: Kikotsoka,
  Neutreeko: Neutreeko
};
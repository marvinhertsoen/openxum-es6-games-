"use strict";

import MoveType from "../../../openxum-core/games/imperium/move_type.mjs";
import OpenXum from '../../../openxum-core/openxum/index.mjs';
import Phase from "../../../openxum-core/games/imperium/phase.mjs";

class RandomPlayer extends OpenXum.Player {
  constructor(c, o, e) {
    super(c, o, e);
  }

// public methods
  confirm() {
    return true;
  }

  is_ready() {
    return true;
  }

  is_remote() {
    return false;
  }

  move() {
    let list = this._engine.get_possible_move_list();

    if (this._engine.phase() === Phase.MOVE) {
      let max = 0;
      let max_index = 0;

      for (let i = 0; i < list.length; ++i) {
        const move = list[i];
        const data = move.data();

        if (data !== null && data.way !== null) {
          const way_type = data.way.type();
          const group = this._engine.group(data.clan, data.group);
          const score = group.move_capability(way_type) / (group.entity_number() * 100);

          if (max < score) {
            max = score;
            max_index = i;
          }
        }
      }
      return list[max_index];
    } else if (this._engine.phase() === Phase.INTERACTION) {
      let max = -100;
      let max_index = 0;

      for (let i = 0; i < list.length; ++i) {
        const move = list[i];
        const data = move.data();

        if (data !== null) {
          const group = this._engine.group(data.clan, data.group);
          const opponent_group = this._engine.group(data.opponent_clan, data.opponent_group);
          const observations = group.observe(opponent_group);
          const entity = group.entity(data.entity);
          const opponent = opponent_group.entity(data.opponent);
          const score = this._compute_score(entity, observations[opponent.id()], entity.interactions()[data.interaction]);

          if (max < score) {
            max = score;
            max_index = i;
          }
        }
      }
      return list[max_index];
    } else {
      return list[Math.floor(Math.random() * list.length)];
    }
  }

  reinit(e) {
    this._engine = e;
  }

  // private methods
  _compute_score(entity, observation, interaction) {
    let score = 0;
    let number = 0;

    Object.keys(observation).forEach((key) => {
      const value = interaction[key];

      if (value > 0) {
        score += (entity.capabilities()[key] - observation[key]) / (100 - value);
        ++number;
      }
    });
    score /= number;
    return score;
  }
}

export default RandomPlayer;
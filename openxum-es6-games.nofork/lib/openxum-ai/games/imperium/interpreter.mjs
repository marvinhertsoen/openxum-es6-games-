"use strict";

require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const Parser = require('jison').Parser;

class Interpreter {
  constructor(engine) {
    this._engine = engine;
  }

// public methods
  move_script() {
//    const script = "(g, w) = arg max[g in move_groups(*), w in out_ways(location(g))](move_capability(g, type(w)))\n" +
//      "if (g exists) [ new move(g, w) ] [ new no_move ]\n";

    let list = this._engine.get_possible_move_list();

    return list[Math.floor(Math.random() * list.length)];
  }

  interaction_script() {
    let list = this._engine.get_possible_move_list();

    return list[Math.floor(Math.random() * list.length)];
  }
}

export default Interpreter;
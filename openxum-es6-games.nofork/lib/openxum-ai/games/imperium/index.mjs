"use strict";

import RandomPlayer from './random_player.mjs';
import ScriptingPlayer from './scripting_player.mjs';

export default {
  RandomPlayer: RandomPlayer,
  ScriptingPlayer: ScriptingPlayer
};
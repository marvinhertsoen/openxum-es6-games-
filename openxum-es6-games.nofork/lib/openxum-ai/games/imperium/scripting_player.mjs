"use strict";

import Interpreter from "./interpreter.mjs";
import OpenXum from '../../../openxum-core/openxum/index.mjs';
import Phase from "../../../openxum-core/games/imperium/phase.mjs";

class ScriptingPlayer extends OpenXum.Player {
  constructor(c, o, e) {
    super(c, o, e);
    this._interpreter = new Interpreter(e);
  }

// public methods
  confirm() {
    return true;
  }

  is_ready() {
    return true;
  }

  is_remote() {
    return false;
  }

  move() {
    if (this._engine.phase() === Phase.MOVE) {
      return this._interpreter.move_script();
    } else if (this._engine.phase() === Phase.INTERACTION) {
      return this._interpreter.interaction_script();
    }
    return null;
  }

  reinit(e) {
    this._engine = e;
  }
}

export default ScriptingPlayer;
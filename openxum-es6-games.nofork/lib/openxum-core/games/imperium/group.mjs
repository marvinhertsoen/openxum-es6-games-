"use strict";

const cost_max = 5;

class Group {
  constructor(clan_index, index) {
    this._clan_index = clan_index;
    this._index = index;
    this._entities = [];
    this._location = null;
    this._interactions = 0;
  }

  add_entity(entity) {
    this._entities.push(entity);
    entity.set_group(this);
  }

  assign_location(location) {
    this._location = location;
    this._location.add_group(this);
  }

  best_entity() {
    let best = null;
    let best_energy = -1;

    this._entities.forEach((e) => {
      if (e.energy() > best_energy) {
        best_energy = e.energy();
        best = e;
      }
    });
    return best;
  }

  can_move() {
    return this._interactions === 0 && this.is_alive();
  }

  clan_index() {
    return this._clan_index;
  }

  energy() {
    let sum = 0;

    this._entities.forEach((e) => {
      sum += e.energy();
    });
    return sum;
  }

  entity(entity_index) {
    let index = 0;
    let found = false;

    while (!found && index < this._entities.length) {
      if (this._entities[index].id() === entity_index) {
        found = true;
      } else {
        ++index;
      }
    }
    if (found) {
      return this._entities[index];
    } else {
      return null;
    }
  }

  entities() {
    return this._entities;
  }

  entity_number() {
    return this._entities.length;
  }

  id() {
    return this._clan_index + '_' + this._index;
  }

  index() {
    return this._index;
  }

  interact() {
    if (this._interactions === 0) {
      this._interactions = 3;
    } else {
      this._interactions--;
    }
  }

  is_alive() {
    return this.energy() > 0;
  }

  location() {
    return this._location;
  }

  move(data) {
    const way_type = data.way.type();
    const capability = this.move_capability(way_type);
    const cost = cost_max * (1.0 - capability / (this.entity_number() * 100));

    this._location.remove_group(this);
    this._location = data.location;
    this._location.add_group(this);
    this._entities.forEach((e) => {
      e.move(cost, way_type);
    });
    this.remove_dead_entities();
  }

  move_capability(way_type) {
    let sum = 0;

    this._entities.forEach((e) => {
      sum += e.move_capability(way_type);
    });
    return sum;
  }

  observe(opponent_group) {
    const perception = this.perception();
    let observations = {};

    opponent_group.entities().forEach((e) => {
      let capabilities = {};

      Object.assign(capabilities, e.capabilities());
      Object.keys(capabilities).forEach((key) => {
        capabilities[key] += this._normal_random(0, perception.standard_deviation) / 10 *
          (Math.random() < 0.5 ? -1 : 1) * (100 - perception.average) / 2;
        if (capabilities[key] < 0) {
          capabilities[key] = 0;
        }
        if (capabilities[key] > 100) {
          capabilities[key] = 100;
        }
      });
      observations[e.id()] = capabilities;
    });
    return observations;
  }

  perception() {
    let max = 0;
    let sum = 0;
    let sum2 = 0;

    this._entities.forEach((e) => {
      if (max < e.perception()) {
        max = e.perception();
      }
      sum += e.perception();
    });
    this._entities.forEach((e) => {
      sum2 += e.perception() * e.perception();
    });

    const average = sum / this._entities.length;
    const standard_deviation = Math.sqrt((sum2 - average * average) / this._entities.length);

    return {
      average: (average + 2 * max) / 3,
      standard_deviation: standard_deviation / (Math.sqrt(max))
    };
  }

  release() {
    this._interactions = 0;
  }

  remove_dead_entities() {
    let index = 0;

    while (index < this._entities.length) {
      if (!this._entities[index].is_alive()) {
        this._entities.splice(index, 1);
      } else {
        ++index;
      }
    }
  }

  to_string() {
    let str = " => group " + this._index + ": ";

    this._entities.forEach((e) => {
      str += e.to_string() + " ";
    });
    str += "< " + this._location + " >";
    return str;
  }

  // private methods

  _normal_random(average, standard_deviation) {
    return average + (this._gauss_random() * standard_deviation);
  }

  _gauss_random() {
    const u = 2 * Math.random() - 1;
    const v = 2 * Math.random() - 1;
    const r = u * u + v * v;

    if (r === 0 || r >= 1) {
      return this._gauss_random();
    }

    const c = Math.sqrt(-2 * Math.log(r) / r);

    return u * c;
  }
}

export default Group;
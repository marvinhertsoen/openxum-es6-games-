"use strict";

const CapabilityType = {
  ADAPTABILITY: 0, SPEED: 1, DEXTERITY: 2, INTELLIGENCE: 3, ENERGY: 4,
  PSYCHE: 5, COMMUNICATION: 6, STRENGTH: 7, STAMINA: 8, HONESTY: 9
};

export default CapabilityType;
"use strict";

require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const CapabilityType = require("../capability_type.mjs").default;
const LocationType = require("../location_type.mjs").default;
const WayType = require("../way_type.mjs").default;

const names = [ "enic", "sincorian", "sink", "ramako", "molo", "asizane", "zicogne", "zatie",
  "magnete", "rokamo", "plous", "sinpina", "coroline", "romonie", "guilonde", "boulane",
  "colenrane", "calone", "malane", "kolori", "volkamoera", "molke", "mamal", "eanal", "vorale",
  "corani", "pasonal", "kilumator", "lezarik", "satale", "moulecool", "telonore", "jetula",
  "reglane", "delone", "leclanole", "hertole", "tellurane", "kalane", "tini", "roulab", "colona",
  "rito", "roumil", "borina", "mousteus", "ratonice", "crayao", "planra", "coralo", "pinle",
  "poilenale", "cilame", "rutale", "marage", "irana", "irone", "riteus", "mayal", "rolina",
  "mouli", "yinako", "chima", "poirina", "narus", "couprotau", "mousto", "ramonali", "myaste",
  "peronis", "parline", "solcol", "dralo", "noralo", "calane", "rocomebe", "colo", "imale",
  "ralie", "rotoni", "milako", "maliko", "ralina", "colni", "ratitum", "maleco", "ralemis",
  "sinkeli", "malitak", "bezox", "cayoma", "veroti", "roumapli", "corino", "sokina", "sorako",
  "mila", "sicalo", "rototif", "xanati"
];

const capabilities = Object.keys(CapabilityType);
const locations = Object.keys(LocationType);
const ways = Object.keys(WayType);

capabilities.splice(capabilities.indexOf("ENERGY"), 1);

var str = "const entities = \"entities = { \" +\n";

names.forEach((name) => {
  let lc = capabilities.slice();
  let ll = locations.slice();
  let lw = ways.slice();
  let c = [];
  let l = [];
  let w = [];
  let sum = 0;
  let sum2 = 0;

  // capabilities
  for (let i = 0; i < 5; ++i) {
    const index = Math.floor(Math.random() * lc.length);
    let value = 0;

    while (value < 2) {
      value = Math.floor(Math.random() * 10);
    }
    sum += value;
    c.push([lc[index], value]);
    lc.splice(index, 1);
  }
  for (let i = 0; i < c.length; ++i) {
    c[i][1] /= sum;
    c[i][1] = Math.floor(c[i][1] * 100);
    sum2 += c[i][1];
  }
  c[c.length - 1][1] += 100 - sum2;

  // energy
  c.push(['ENERGY', Math.floor(Math.random() * 50) + 50]);

  // locations
  sum = 0;
  sum2 = 0;
  for (let i = 0; i < 3; ++i) {
    const index = Math.floor(Math.random() * ll.length);
    let value = 0;

    while (value < 2) {
      value = Math.floor(Math.random() * 10);
    }
    sum += value;
    l.push([ll[index], value]);
    ll.splice(index, 1);
  }
  for (let i = 0; i < l.length; ++i) {
    l[i][1] /= sum;
    l[i][1] = Math.floor(l[i][1] * 100);
    sum2 += l[i][1];
  }
  l[l.length - 1][1] += 100 - sum2;

  // ways
  sum = 0;
  sum2 = 0;
  for (let i = 0; i < 3; ++i) {
    const index = Math.floor(Math.random() * lw.length);
    let value = 0;

    while (value < 2) {
      value = Math.floor(Math.random() * 10);
    }
    sum += value;
    w.push([lw[index], value]);
    lw.splice(index, 1);
  }
  for (let i = 0; i < w.length; ++i) {
    w[i][1] /= sum;
    w[i][1] = Math.floor(w[i][1] * 100);
    sum2 += w[i][1];
  }
  w[w.length - 1][1] += 100 - sum2;

  // interations
  let its = [];

  for(let k = 0; k < 3; ++k) {
    let it = [];

    lc = capabilities.slice();
    sum = 0;
    sum2 = 0;
    for (let i = 0; i < 2; ++i) {
      const index = Math.floor(Math.random() * lc.length);
      let value = 0;

      while (value < 2) {
        value = Math.floor(Math.random() * 10);
      }
      sum += value;
      it.push([lc[index], value]);
      lc.splice(index, 1);
    }
    for (let i = 0; i < it.length; ++i) {
      it[i][1] /= sum;
      it[i][1] = Math.floor(it[i][1] * 100);
      sum2 += it[i][1];
    }
    it[it.length - 1][1] += 100 - sum2;
    its.push(it);
  }
  // string
  str += "\"" + name + ": [ ";

  for (let i = 0; i < c.length; ++i) {
    str += c[i][0] + " = " + c[i][1];
    if (i !== c.length - 1) {
      str += ", ";
    }
  }
  str += "]\"\n + \"[";
  for (let i = 0; i < w.length; ++i) {
    str += w[i][0] + " = " + w[i][1];
    if (i !== w.length - 1) {
      str += ", ";
    }
  }
  str += "]\"\n + \"[";
  for (let i = 0; i < l.length; ++i) {
    str += l[i][0] + " = " + l[i][1];
    if (i !== l.length - 1) {
      str += ", ";
    }
  }
  str += "]\"\n + \"[";
  for (let k = 0; k < its.length; ++k) {
    const it = its[k];

    str += "< ";
    for (let i = 0; i < it.length; ++i) {
      str += it[i][0] + " = " + it[i][1];
      if (i !== it.length - 1) {
        str += ", ";
      }
    }
    str += " > ";
  }
  str += "] < " + (Math.floor(Math.random() * 100)) +" >\" +\n";

});

str += "\"}\";";

console.log(str);
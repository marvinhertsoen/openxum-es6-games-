"use strict";

import Builder from './builder.mjs';
import Color from './color.mjs';
import Entities from './data/entities.mjs';
import Entity from './entity.mjs';
import OpenXum from '../../openxum/index.mjs';
import Move from './move.mjs';
import MoveType from './move_type.mjs';
import Phase from './phase.mjs';

class Engine extends OpenXum.Engine {
  constructor(t, c) {
    super();
//    Math.seedrandom(1005);
    this._type = t;
    this._current_color = c;
    this._load_entities();
    this._builder = new Builder();
    this._build_clans(t + 2);
    this._build_map(10);
    this._assign_locations_to_clans();
    this._phase = Phase.MOVE;
  }

// public methods
  apply_moves(moves) {
    // TODO
  }

  clone() {
    // TODO
  }

  current_clan() {
    return this._clans[this._current_color];
  }

  current_color() {
    return this._current_color;
  }

  get_name() {
    return 'Imperium';
  }

  get_possible_move_list() {
    let list = [];
    const current_clan = this._clans[this._current_color];
    const groups = current_clan.groups();

    if (this._phase === Phase.MOVE) {
      for (let i = 0; i < groups.length; ++i) {
        const group = groups[i];
        const current_location = group.location();

        if (group.can_move()) {
          for (let k = 0; k < this._map.locations().length; ++k) {
            const way = this._map.get_way(current_location.index(), k);
            const new_location = this._map.locations()[k];

            if (current_location.index() !== k && way !== null && !new_location.is_clan_present(group.clan_index())) {
              list.push(new Move(MoveType.MOVE, this._current_color,
                {
                  clan: group.clan_index(),
                  group: group.index(),
                  location: new_location,
                  way: way
                }));
            }
          }
        }
        if (current_clan.possible_no_move()) {
          list.push(new Move(MoveType.MOVE, this._current_color, null));
        }
      }
      if (list.length === 0) {
        list.push(new Move(MoveType.ABORT, this._current_color, null));
      }
    } else if (this._phase === Phase.INTERACTION) {
      for (let i = 0; i < groups.length; ++i) {
        const group = groups[i];
        const presents = group.location().groups();

        if (presents.length > 1) {
          for (let m = 0; m < presents.length; ++m) {
            const opponent_group = presents[m];

            if (opponent_group.clan_index() !== group.clan_index()) {
              const entities = group.entities();

              for (let j = 0; j < entities.length; ++j) {
                const entity = entities[j];

                for (let k = 0; k < entity.interactions().length; ++k) {
                  const opponents = opponent_group.entities();

                  for (let p = 0; p < opponents.length; ++p) {
                    list.push(new Move(MoveType.INTERACTION, this._current_color,
                      {
                        clan: group.clan_index(),
                        group: group.index(),
                        entity: entity.id(),
                        opponent_clan: opponent_group.clan_index(),
                        opponent_group: opponent_group.index(),
                        opponent: opponents[p].id(),
                        interaction: k
                      }));
                  }
                }
              }
            }
          }
        }
      }
      if (list.length === 0) {
        list.push(new Move(MoveType.INTERACTION, this._current_color, null));
      }
    }
    return list;
  }

  get_type() {
    return this._type;
  }

  group(clan_index, group_index) {
    return this._clans[clan_index].group(group_index);
  }

  is_finished() {
    if (this._phase === Phase.ABORTED) {
      return true;
    } else {
      let n = 0;

      for (let i = 0; i < this._clans.length; ++i) {
        if (this._clans[i].energy() === 0) {
          ++n;
        }
      }
      return n >= this._clans.length - 1;
    }
  }

  map() {
    return this._map;
  }

  move(move) {
    if (move.type() === MoveType.MOVE) {
      let current_clan = this._clans[this._current_color];

      if (move.data() === null) {
        current_clan.pass();
      } else {
        let group = current_clan.group(move.data().group);

        current_clan.reset_pass();
        group.move(move.data());
        if (!group.is_alive()) {
          this._remove_group(group);
        }
      }
      this._phase = Phase.INTERACTION;
    } else if (move.type() === MoveType.INTERACTION) {
      let current_clan = this._clans[this._current_color];

      if (move.data() !== null) {
        let group = current_clan.group(move.data().group);
        let entity = group.entity(move.data().entity);
        let opponent_clan = this._clans[move.data().opponent_clan];
        let opponent_group = opponent_clan.group(move.data().opponent_group);
        let opponent = opponent_group.entity(move.data().opponent);

        current_clan.reset_pass();
        group.interact();
        opponent_group.interact();
        entity.interact(opponent, move.data().interaction);
        group.remove_dead_entities();
        opponent_group.remove_dead_entities();
        if (!group.is_alive() || !opponent_group.is_alive()) {
          group.release();
          opponent_group.release();
          if (!group.is_alive()) {
            this._remove_group(group);
          }
          if (!opponent_group.is_alive()) {
            this._remove_group(opponent_group);
          }
        }
      } else {
        current_clan.pass();
      }
      this._current_color = this._current_color === Color.BLACK ? Color.WHITE : Color.BLACK;
      this._phase = Phase.MOVE;
    } else if (move.type() === MoveType.ABORT) {
      this._clans[this._current_color].abort();
      this._phase = Phase.ABORTED;
    }
  }

  parse(str) {
    // TODO
  }

  phase() {
    return this._phase;
  }

  to_string() {
    // TODO
  }

  winner_is() {
    if (this._phase === Phase.ABORTED) {
      let found = false;
      let i = 0;

      while (i < this._clans.length && !found) {
        if (this._clans[i].aborted()) {
          found = true;
        } else {
          ++i;
        }
      }
      if (found) {
        let j = 0;

        found = false;
        while (j < this._clans.length && !found) {
          if (i !== j && this._clans[j].energy() > 0) {
            found = true;
          } else {
            ++j;
          }
        }

//        console.log("BEST = " + this._clans[i].best_entity().to_string());

        if (found) {
          return j;
        } else {
          return -1;
        }
      } else {
        return -1;
      }
    } else {
      let found = false;
      let i = 0;

      while (i < this._clans.length && !found) {
        if (this._clans[i].energy() > 0) {
          found = true;
        } else {
          ++i;
        }
      }

//      console.log("BEST = " + this._clans[i].best_entity().to_string());

      if (found) {
        return i;
      } else {
        return -1;
      }
    }
  }

// private methods
  _build_clans(number) {
    let entities = this._entities.slice();

    this._clans = [];
    for (let i = 0; i < number; ++i) {
      this._clans.push(this._builder.build_clan(i, entities));
    }
  }

  _build_map(location_number) {
    this._map = this._builder.build_map(location_number);
  }

  _assign_locations_to_clans() {
    let indexes = [];

    for (let k = 0; k < this._map.locations().length; ++k) {
      indexes.push(k);
    }
    for (let i = 0; i < this._clans.length; ++i) {
      const index = Math.floor(Math.random() * indexes.length);

      for (let j = 0; j < this._clans[i].groups().length; ++j) {
        this._clans[i].assign_location_to_group(this._map.locations()[indexes[index]], j);
      }
      indexes.splice(index, 1);
    }
  }

  _load_entities() {
    let parser = Entities.generate();

    parser.yy.Entity = Entity;
    parser.yy.entities = [];
    parser.yy.capabilities = [];
    parser.yy.move_capabilities = [];
    parser.yy.location_capabilities = [];
    parser.yy.interactions = [];
    parser.yy.interaction = [];
    parser.yy.perception = 0;
    parser.parse(Entities.entities);
    this._entities = parser.yy.entities;
  }

  _remove_group(group) {
    this._clans[group.clan_index()].remove_group(group);
    group.location().remove_group(group.index());
  }
}

export default Engine;
"use strict";

import CapabilityType from "./capability_type.mjs";
import LocationType from "./location_type.mjs";
import WayType from "./way_type.mjs";

const alpha_1 = 0.3;
const alpha_2 = 0.5;
const alpha_3 = 0.2;

class Entity {
  constructor(id) {
    this._id = id;
    this._group = null;
    this._capabilities = {};
    Object.keys(CapabilityType).forEach((key) => {
      this._capabilities[key] = 0;
    });
    this._perception = 0;
    this._location_capabilities = {};
    Object.keys(LocationType).forEach((key) => {
      this._location_capabilities[key] = 0;
    });
    this._move_capabilities = {};
    Object.keys(WayType).forEach((key) => {
      this._move_capabilities[key] = 0;
    });
    this._interactions = [];
  }

  capabilities() {
    return this._capabilities;
  }

  clone() {
    let o = new Entity(this._id);

    Object.assign(o, this);
    return o;
  }

  energy() {
    return this._capabilities.ENERGY;
  }

  id() {
    return this._id;
  }

  interact(opponent, interaction) {

//    console.log("EFFECT = ", this._id, "vs", opponent._id);

    const location = this._group.location().type();
    const location_score = 0.7 * this._location_capabilities[location] + 0.3 * this._capabilities.ADAPTABILITY;
    const opponent_location_score = 0.7 * opponent._location_capabilities[location] + 0.3 * this._capabilities.ADAPTABILITY;
    const factor = (0.15 - 0.015) * (alpha_1 * this._capabilities.INTELLIGENCE +
      alpha_2 * this._capabilities.PSYCHE + alpha_3 * location_score) / 100 + 0.015;
    const opponent_factor = (0.15 - 0.015) * (alpha_1 * opponent._capabilities.INTELLIGENCE +
      alpha_2 * opponent._capabilities.PSYCHE + alpha_3 * opponent_location_score) / 100 + 0.015;

    Object.keys(this._interactions[interaction]).forEach((key) => {
      const value = this._interactions[interaction][key];

      if (value > 0 && opponent._capabilities[key] > 0) {
        const e = value / 100;
        const effect = 0.5 * (1.0 + Math.tanh((e - 0.5) / factor));
        const defense = 1.0 - 0.5 * (1.0 + Math.tanh((e - 0.5) / opponent_factor));

//        console.log("EFFECT = ", key, value, effect / defense);
//        console.log("EFFECT [BEFORE] = ", opponent._capabilities[key], opponent._capabilities.ENERGY);

        if (effect > 0) {
          opponent._capabilities[key] -= value * effect / defense;
          if (opponent._capabilities[key] < 0) {
            opponent._capabilities[key] = 0;
          }
          this._remove_energy(0.1 * value * effect);
          opponent._remove_energy(0.1 * value * defense);
        }

//        console.log("EFFECT [AFTER] = ", opponent._capabilities[key], opponent._capabilities.ENERGY);

      }
    });
  }

  interactions() {
    return this._interactions;
  }

  is_alive() {
    return this.energy() > 0;
  }

  move(cost, way_type) {
    this._remove_energy(cost * (1.0 - this._move_capabilities[way_type] / 100));
  }

  move_capability(type) {
    return this._move_capabilities[type];
  }

  perception() {
    return this._perception;
  }

  set_capabilities(list) {
    list.forEach((item) => {
      if (Object.keys(CapabilityType).indexOf(item._type) !== -1) {
        this._capabilities[item._type] = item._level;
      }
    });
  }

  set_group(group) {
    this._group = group;
  }

  set_interactions(list) {
    list.forEach((item) => {
      let interaction = {};

      Object.keys(CapabilityType).forEach((key) => {
        interaction[key] = 0;
      });
      item.forEach((e) => {
        if (Object.keys(CapabilityType).indexOf(e._type) !== -1) {
          interaction[e._type] = e._level;
        }
      });
      this._interactions.push(interaction);
    });
  }

  set_location_capabilities(list) {
    list.forEach((item) => {
      if (Object.keys(LocationType).indexOf(item._type) !== -1) {
        this._location_capabilities[item._type] = item._level;
      }
    });
  }

  set_move_capabilities(list) {
    list.forEach((item) => {
      if (Object.keys(WayType).indexOf(item._type) !== -1) {
        this._move_capabilities[item._type] = item._level;
      }
    });
  }

  set_perception(value) {
    this._perception = value;
  }

  to_string() {
    return this._id + " (" + this.energy() + ")";
  }

  // private methods
  _remove_energy(value) {
    this._capabilities.ENERGY -= value;
    if (this._capabilities.ENERGY < 0) {
      this._capabilities.ENERGY = 0;
    }
  }
}

export default Entity;
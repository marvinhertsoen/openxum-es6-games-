"use strict";

class Way {
  constructor(type, from, to) {
    this._type = type;
    this._from = from;
    this._to = to;
  }

  from() {
    return this._from;
  }

  to() {
    return this._to;
  }

  to_string() {
    return this._from + " -> " + this._to + " <" + this._type + ">";
  }

  type() {
    return this._type;
  }
}

export default Way;
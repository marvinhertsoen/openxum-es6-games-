"use strict";

class Clan {
  constructor(c) {
    this._color = c;
    this._groups = [];
    this._exchange_credits = 0;
    this._alliances = [];
    this._pass = 0;
    this._aborted = false;
  }

  abort() {
    this._aborted = true;
  }

  aborted() {
    return this._aborted;
  }

  add_group(group) {
    this._groups.push(group);
  }

  assign_location_to_group(location, group) {
    this._groups[group].assign_location(location);
  }

  best_entity() {
    let best_of_best = null;
    let best_energy = -1;

    this._groups.forEach((g) => {
      let best = g.best_entity();

      if (best.energy() > best_energy) {
        best_energy = best.energy();
        best_of_best = best;
      }
    });
    return best_of_best;
  }

  can_move_groups() {
    let list = [];

    this._groups.forEach((g) => {
      if (g.can_move()) {
        list.push(g);
      }
    });
    return list;
  }

  color() {
    return this._color;
  }

  energy() {
    let sum = 0;

    this._groups.forEach((g) => {
      sum += g.energy();
    });
    return sum;
  }

  group(group_index) {
    let index = 0;
    let found = false;

    while (!found && index < this._groups.length) {
      if (this._groups[index].index() === group_index) {
        found = true;
      } else {
        ++index;
      }
    }
    if (found) {
      return this._groups[index];
    } else {
      return null;
    }
  }

  groups() {
    return this._groups;
  }

  pass() {
    this._pass++;
  }

  possible_no_move() {
    return this._pass < 3;
  }

  remove_group(group) {
    let index = 0;
    let found = false;

    while (!found && index < this._groups.length) {
      if (this._groups[index].id() === group.id()) {
        found = true;
      } else {
        ++index;
      }
    }
    if (found) {
      this._groups.splice(index, 1);
    }
  }

  reset_pass() {
    this._pass = 0;
  }

  to_string() {
    let str = "clan - " + this._color + " -\n";

    this._groups.forEach((g) => {
      str += g.to_string() + "\n";
    });
    return str;
  }
}

export default Clan;
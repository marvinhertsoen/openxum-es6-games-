"use strict";

let Phase = {
  MOVE: 0,
  NEGOTIATION: 1,
  INTERACTION: 2,
  ABORTED: 3
};

export default Phase;
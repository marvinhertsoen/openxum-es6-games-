"use strict";

import Color from "./color.mjs";
import OpenXum from '../../openxum/index.mjs';
import MoveType from "./move_type.mjs";

class Move extends OpenXum.Move {
  constructor(t, c, d) {
    super();
    this._type = t;
    this._color = c;
    this._data = d;
  }

// public methods
  data() {
    return this._data;
  }

  decode(str) {
    // TODO
  }

  encode() {
    // TODO
  }

  to_object() {
    // TODO
  }

  to_string() {
    if (this._type === MoveType.MOVE) {
      if (this._data === null) {
        return 'no move ' + (this._color === Color.BLACK ? 'black' : 'white');
      } else {
        return 'move ' + (this._color === Color.BLACK ? 'black' : 'white') + ' group ' + this._data.group +
          ' to < ' + this._data.location.index() + ', ' + this._data.way.type() + ' >';
      }
    } else if (this._type === MoveType.INTERACTION) {
      if (this._data === null) {
        return 'make no interaction ' + (this._color === Color.BLACK ? 'black' : 'white');
      } else {
        return 'make interaction ' + (this._color === Color.BLACK ? 'black' : 'white') + " " + this._data.group + "::" +
          this._data.entity + " with " + this._data.opponent_group + "::" + this._data.opponent;
      }
    } else if (this._type === MoveType.ABORT) {
      return 'aborted by ' + (this._color === Color.BLACK ? 'black' : 'white');
    }
  }

  type() {
    return this._type;
  }
}

export default Move;
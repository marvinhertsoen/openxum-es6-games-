"use strict";

class Location {
  constructor(index, t) {
    this._index = index;
    this._type = t;
    this._groups = [];
  }

  add_group(group) {
    this._groups.push(group);
  }

  index() {
    return this._index;
  }

  is_clan_present(clan_index) {
    let found = false;
    let index = 0;

    while (!found && index < this._groups.length) {
      if (this._groups[index].clan_index() === clan_index && this._groups[index].is_alive()) {
        found = true;
      } else {
        ++index;
      }
    }
    return found;
  }

  groups() {
    return this._groups;
  }

  remove_group(group) {
    this._groups.splice(this._groups.indexOf(group), 1);
  }

  to_string() {
    let str = "";

    str += this._index + " <" + this._type + "> [ ";
    this._groups.forEach((g) => {
      str += g.id() + " ";
    });
    str += "]";
    return str;
  }

  type() {
    return this._type;
  }
}

export default Location;
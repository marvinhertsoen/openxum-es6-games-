"use strict";

let Color = {
  NONE: -1, BLACK: 0, WHITE: 1, RED: 2, GREEN: 3, BLUE: 4, ORANGE: 5,
  YELLOW: 6, GREY: 7, PINK: 8, PURPLE: 9
};

export default Color;